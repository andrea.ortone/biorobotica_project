#include <iostream>
#include <vector>
#include <random>
#include <math.h>
#include <time.h>
#include <fstream>


using namespace std;
unsigned N=1000;
float mean_signal = 12.;
float T=5000, step_t=0.1;
float neglect_margin=100;


float   t_g_r=0.5, t_g_d=5, lag_g = 1;
float   t_a_r=0.5, t_a_d=2, lag_a = 1;
float   V_a=70, V_res = 11, V_th=18, ref_t = 1, t_m=10; // solo neuroni inibitori!
float   j_g=4./20, j_a=0.3/20;   // già diviso per 20 nS

class Neuron{
public:
    vector<int> neig;
    vector<float> t_i, t_e, t_spikes;
    float V=0, locked_until=0;
    Neuron(float ratio, mt19937& g, uniform_real_distribution<float>& dis) {
        float t=0;
        for (unsigned i=0; i<N; i++) {
            if (dis(g) < ratio) neig.push_back(i);
        }
        while(t<T) {
            t += -log(1-dis(g))/mean_signal;
            t_e.push_back(t);
        }
        cout << t_e.size() << endl;
    }
    float step_function(float x, int& check){
        if (x < 0) {
            check=1;
            return 0;
        }
        return x;
    }
    float f(float x, float y, vector<float>& ts_i, vector<float>& ts_e) {
        float temp1=0, temp2=0;
        int check=0;
        for(unsigned i=0; i<ts_e.size(); i++) {
            temp1 += exp(-step_function(x-ts_e[i]-lag_a, check)/t_a_d)-exp(-step_function(x-ts_e[i]-lag_a, check)/t_a_r);
            if (check==1) break;
        }
        for(unsigned i=0; i<ts_i.size(); i++) {
            temp2 += exp(-step_function(x-ts_i[i]-lag_g, check)/t_g_d)-exp(-step_function(x-ts_i[i]-lag_g, check)/t_g_r);
        }
        return -y/t_m - j_a*(y-V_a)*temp1/(t_a_d-t_a_r) - j_g*y*temp2/(t_g_d-t_g_r);
    }
    float RungeKutta(vector<float>& ts_i, vector<float>& ts_e, float x0, float y0, float h) {
        float k1, k2, k3, k4;
        float y = y0;
        k1 = h*f(x0, y, ts_i, ts_e);
        k2 = h*f(x0 + 0.5*h, y + 0.5*k1, ts_i, ts_e);
        k3 = h*f(x0 + 0.5*h, y + 0.5*k2, ts_i, ts_e);
        k4 = h*f(x0 + h, y + k3, ts_i, ts_e);
        y = y + (1.0/6.0)*(k1 + 2*k2 + 2*k3 + k4);
        return y;
    }
    void neglect_past(float t) {
        unsigned i;
        for(i=0; i!=t_e.size(); i++) {
            // if (i!=0) cout << i << " t " << t << " t_e[i] " << t_e[i] << " " ;
            if (t-t_e[i]<neglect_margin) break;
        }
        // cout << i << "\n";
        if (i!=0) t_e.erase(t_e.begin(),t_e.begin()+i-1);
        for(i=0; i!=t_i.size(); i++) {
            // if (i!=0) cout << i << " t " << t << " t_e[i] " << t_e[i] << " " ;
            if (t-t_i[i]<neglect_margin) break;
        }
        if (i!=0) t_i.erase(t_i.begin(),t_i.begin()+i-1);
    }
};

class Network{
private:
    float    ratio;

public:
    vector<Neuron>  net;
    Network(float r, mt19937 g){
        uniform_real_distribution<float>  dis(0,1);
        ratio = r;
        for (unsigned i=0; i<N; i++) {
            net.push_back(Neuron(r, g, dis));
        }
    }
    void step(float t) {
        for (unsigned i=0; i<N; i++) {
            net[i].V = net[i].RungeKutta(net[i].t_i, net[i].t_e, t, net[i].V, step_t);
            net[i].neglect_past(t);
            if (net[i].V > V_th && net[i].locked_until<t) {
                net[i].V = V_res;
                net[i].t_spikes.push_back(t);
                for(unsigned j=0; j<net[i].neig.size(); j++) {
                    net[net[i].neig[j]].t_i.push_back(t);
                }
                net[i].locked_until=t+ref_t;
            }
        }
    }
};


int find_index(float t, float margin, vector<float>& v) {
    unsigned i;
    for(i=0; i!=v.size(); i++) {
        if (t-v[i]<margin) break;
    }
    return i-1;
}

int main ()
{
    mt19937 gen(time(0));
    Network n(0.2, gen);

    float t=0;
    unsigned steps=int(T/step_t);

    ofstream       outFile2("spikes.txt");
    ofstream       outFile("v.txt");

    for (unsigned i=0; i<steps; i++) {
        t += step_t;
        n.step(t);
        if (i%int(steps/100)==0) cout << (i*100)/steps << " %\n";
        outFile << t << " " << n.net[0].V << " " << n.net[100].V << " " << n.net[200].V  << endl;
    }
    outFile.close();

    for(unsigned j=0; j<N; j++) {
        for(unsigned i=0; i<n.net[j].t_spikes.size(); i++) {
            outFile2 << n.net[j].t_spikes[i] << " ";
        }
        outFile2 << endl;
    }
    outFile2.close();

    return 0;
}
