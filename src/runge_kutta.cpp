#include <iostream>
#include <math.h>
#include <vector>
#include <fstream>

using namespace std;

float   t_g_r=0.5, t_g_d=5, lag_g = 1;
float   t_a_r=0.5, t_a_d=2, lag_a = 1;
float   V_a=70, V_res = 11, V_th=18, ref_t = 1, t_m=10; // solo neuroni inibitori!
float   j_g=4./20, j_a=0.3/20;   // già diviso per 20 nS

float step_function(float x){
    if (x < 0) return 0;
    return x;
}

float f(float x, float y, vector<float>& ts_i, vector<float>& ts_e) {
    float temp1=0, temp2=0;
    for(unsigned i=0; i<ts_e.size(); i++) {
        temp1 += exp(-step_function(x-ts_e[i]-lag_a)/t_a_d)-exp(-step_function(x-ts_e[i]-lag_a)/t_a_r);
    }
    for(unsigned i=0; i<ts_i.size(); i++) {
        temp2 += exp(-step_function(x-ts_i[i]-lag_g)/t_g_d)-exp(-step_function(x-ts_i[i]-lag_g)/t_g_r);
    }
    return -y/t_m - j_a*(y-V_a)*temp1/(t_a_d-t_a_r) - j_g*y*temp2/(t_g_d-t_g_r);
}

float RungeKutta(vector<float>& ts_i, vector<float>& ts_e, float& x0, float y0, float h)
{
    float k1, k2, k3, k4;
    float y = y0;

    k1 = h*f(x0, y, ts_i, ts_e);
    k2 = h*f(x0 + 0.5*h, y + 0.5*k1, ts_i, ts_e);
    k3 = h*f(x0 + 0.5*h, y + 0.5*k2, ts_i, ts_e);
    k4 = h*f(x0 + h, y + k3, ts_i, ts_e);

    y = y + (1.0/6.0)*(k1 + 2*k2 + 2*k3 + k4);

    x0 = x0 + h;


    return y;
}

// Driver method
int main()
{
    float           V=0, t=0, delta_t=0.1;
    ofstream        outFile("prova.txt");
    vector<float>   t_i, t_e{1,10};
    for (unsigned i=0; i<10000; i++){
        V=RungeKutta(t_i, t_e, t, V, delta_t);
        outFile << i*delta_t << " " << V << endl;
    }
    outFile.close();
    return 0;
}
