import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq

plt.style.use('style.yml')

def read_spikes(file):
    l=[]
    with open(file) as in_file:
        for line in in_file.readlines():
            f_list = [float(i) for i in line.split()]
            l.append(f_list)
    return l

t,V1,V2,V3 = np.loadtxt('v4.txt', unpack=True)
l = read_spikes('spikes4.txt')
flat_list = [item for sublist in l for item in sublist]

# V1 = 0    V2=100 V3=200
plt.plot(t,V3)
plt.xlim((1400,1600))
for t in l[200]:
    plt.plot([t]*2, [min(V3), max(V3)], color='black')
plt.xlabel('t [ms]')
plt.ylabel(r'$V_m [mV]$')
# plt.savefig('V3.png')
plt.show()

plt.xlabel('t [ms]')
plt.figure(figsize=(8,2))
plt.eventplot(l[10:20])
plt.xlim((1400,1600))
plt.savefig('raster.png')
plt.show()

bins = 3500
y, bin_ticks, _2 = plt.hist(flat_list, bins=bins)
plt.xlim((1400,1600))
plt.ylim((0,80))
plt.ylabel(r'$\nu(t)$')
plt.xlabel('t [ms]')
plt.savefig('hist.png')
plt.show()

y=y[60:]

yf = fft(y)
yfft = yf[1:int(len(yf)/2)]**2
xfft = fftfreq(len(y), (bin_ticks[1]-bin_ticks[0])/1000)[1:int(len(yf)/2)]
yfft = np.convolve(yfft, np.ones(1), 'valid') / 1
plt.plot(xfft[:len(yfft)], yfft.real)
plt.plot(xfft[:len(yfft)], yfft.imag)
plt.show()
plt.plot(xfft[:len(yfft)], abs(yfft), linewidth=0.6)
plt.show()


N = 4
len = len(y)
y_p = np.array([y[i*int(len/N):(i+1)*int(len/N)] for i in range(N)])
temp = np.array([fft(yp) for yp in y_p])

ffts = temp[:,1:int(temp.shape[1]/2)]
xffts = fftfreq(y_p.shape[1], (bin_ticks[1]-bin_ticks[0])/1000)[1:int(y_p.shape[1]/2)]

for ff in ffts:
    plt.plot(xffts, abs(ff)**2, linewidth = 0.6)
plt.show()

means=[abs((ffts[:,f])**2).mean() for f in range(ffts.shape[1])]
plt.plot(xffts, means)
plt.show()
means = np.convolve(means, np.ones(3), 'valid') / 3
plt.plot(xffts[:means.shape[0]], means)
plt.xlabel('f [Hz]')
plt.ylabel(r'power spectrum [u.a.]')
plt.xlim((0,300))
# plt.savefig('fft.png')
plt.show()
